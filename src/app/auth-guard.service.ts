import { Injectable }       from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService }      from './user.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  	
  constructor(private authService: UserService, private router: Router) {
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        let url: string = state.url;
	console.log('Url:'+ url);
	if (this.authService.getUserLogin()) {
		return true; 
	}
   console.log('need to authendicate'); 
  this.router.navigate(['']);
	return false;
  }
}
