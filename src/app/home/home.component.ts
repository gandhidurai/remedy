import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router'
import { error } from '@angular/compiler/src/util';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  
  incidentForm: any;
  constructor(private formBuilder: FormBuilder,private userService: UserService,private router: Router) { 

    this.incidentForm = this.formBuilder.group({
    
      'incident_id': [''],
      'company': ['', Validators.required],
      'short_dec': [''],
      'impact': ['', Validators.required],
      'urgency': ['', Validators.required],
      'reported_source': ['', Validators.required],
      'service_type': ['', Validators.required],
      'status': ['', Validators.required],
      'assignee': ['', Validators.required],
      'submitter': [''],
      'create_date': [''],
      'modify_date': [''],
      'last_modify_date': [''],
      'oc_tier_1': ['', Validators.required],
      'oc_tier_2': ['', Validators.required],
      'oc_tier_3': ['', Validators.required],
      'pc_tier_1': ['', Validators.required],
      'pc_tier_2': ['', Validators.required],
      'pc_tier_3': ['', Validators.required],

      'w_dec': [''],
      'w_time': [''],
      'type': ['', Validators.required]

     });
  }

  ngOnInit() {
  }

  impacts: any[]=["Extensive/Widespread","Significant/Large","Moderate/Limited","Minor/Localized"];

  urgencys: any[]=["Critical","High","Medium","Low"];

  reported_sources: any[]=["Direct Input","Email","External Escalation","Other"];

  service_types: any[]=["User Service Restoration","User Service Request","Infrastructure Restoration","Infrastructure Event"];
  statues: any[]=["New"];

  onIncidentSubmit(){
    console.info(`${this.incidentForm.value.company}`+','+
    `${this.incidentForm.value.short_dec}`+','+
    `${this.incidentForm.value.impact}`+','+
    `${this.incidentForm.value.priority}`+','+
    `${this.incidentForm.value.status}`+','+
    `${this.incidentForm.value.submitter}`+','+
    `${this.incidentForm.value.oc_tier_1}`+','+
    `${this.incidentForm.value.oc_tier_2}`+','+
    `${this.incidentForm.value.oc_tier_3}`+','+
    `${this.incidentForm.value.pc_tier_1}`+','+
    `${this.incidentForm.value.pc_tier_2}`+','+
    `${this.incidentForm.value.pc_tier_3}`+','+
    `${this.incidentForm.value.w_dec}`+','+
    `${this.incidentForm.value.type}`+','
    );

    this.filterSubmit(`${this.incidentForm.value.short_dec}`,`${this.incidentForm.value.impact}`,`${this.incidentForm.value.urgency}`,`${this.incidentForm.value.status}`,`${this.incidentForm.value.reported_source}`,`${this.incidentForm.value.service_type}`)
    alert("successfully submitted");
  }




  filterSubmit(dec: string,impact: string,urgency: string,status: string,reported_source: string,service_type: string){
  this.userService.createSubmit(dec,impact,urgency,status,reported_source,service_type).subscribe(data => { 
    alert("success "+data)
});
  }

  names: any[] =["option 1","option 2","option 3","option 4","option 5"];

 



 



}
