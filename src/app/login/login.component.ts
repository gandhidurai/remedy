import { Component, OnInit,Injectable } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router'
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

@Injectable()
export class LoginComponent implements OnInit {



  ngOnInit() {
    
 
  }

  public user: User;


  userForm: any;
  name: any;
  nameValidation: string='';

 constructor(private userService: UserService,private formBuilder: FormBuilder,private router: Router) { 
 
  this.userForm = this.formBuilder.group({
    'username': ['', Validators.required],
    'password': ['', Validators.required]
   });

 }

 
 
onFormSubmit() {
        this.filterUser(`${this.userForm.value.username}`,`${this.userForm.value.password}`);
      
         }

 filterUser(username: string, password: string) {


  this.userService.filterUsersUsingPost(username, password)
     .subscribe(data => { 
      this.user = data['user'];
      this.userService.username=this.user.name;
      this.userService.firstname=this.user.fistname;
      this.userService.lastname=this.user.lastname;
    
      if(data['success']=='success'){
        this.router.navigate(['view']);
        this.userService.setUserLogin();
        
      }
      else{
        this.nameValidation='please provide currect username and password';
      }

      
       },
        error=>{
          this.nameValidation='Internal Server problem please contact admin';
        }
         
        
        
        
        );  
 }

 isValidationClear(){
  this.nameValidation='';
 }


}
