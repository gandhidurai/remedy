import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private userService : UserService) { }

  ngOnInit() {
   console.info(this.userService.username);
   console.info(this.userService.getUserLogin());
  }

userName=this.userService.username;
logOut(){
  this.userService.unSetUserLogin();
}


 

  
}
