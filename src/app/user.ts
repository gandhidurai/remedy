export interface User{
    name: string;
    password: string;
    token: string;
    fistname: string;
    lastname: string;
}